export default class Component{
	tagName;
	attribute;
	children;
	constructor( tagName, attribute, children ) {
		this.tagName=tagName
		this.children=children;
		this.attribute=attribute;
	}
	render(){
		if(this.tagName == 'img'){
			return `<${this.tagName} ${this.attribute.name}='${this.attribute.value}' ${this.children} />	`;
		}
		return `<${this.tagName}>${this.children}</${this.tagName}>`;
	}
}